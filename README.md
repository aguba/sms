# Slot Machine Testing Project

This is the project for testing Slot Machine.

# Dependencies
Make sure you have latest Java, Maven, Chrome and ChromeDriver installed on your system, if not - please follow the vendor instructions for installing them on your operating system.

# Running Tests
Please use the following steps for running tests locally on your machine:

1. Clone this repository to your local machine.
2. Open up a terminal and navigate to the root directory of the repository.
3. Run mvn clean test -Dwebdriver.chrome="PATH_TO_CHROMEDRIVER, example: D:\drivers\chromedriver.exe"

# Test Coverage
- Loss scenario
- Win scenario
- Bet changing scenarios
- Change background, icons, machines
- Check Rest response

The main reason of this project is to demonstrate the ability to automate tests, code styling, approach, architectural solution, etc. Keep please in mind that all test cases were created with an assumption that the application functions as designed.
