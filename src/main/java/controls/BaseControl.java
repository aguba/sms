package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

public class BaseControl {

  protected String name;
  protected By locator;
  protected BaseDriver driver;

  public BaseControl(String name, By locator, BaseDriver driver) {
    this.name = name;
    this.locator = locator;
    this.driver = driver;
  }

  public WebElement getVisibleElement() {
    return getVisibleElement(driver.getTimeout());
  }

  public WebElement getVisibleElement(int sec) {
    WebElement e = driver.getVisibleElement(locator, sec);
    if (e == null) {
      throw new ElementNotVisibleException("Element with locator " + locator + " is NOT visible.");
    }
    return e;
  }

  public String getText() {
    return getText(driver.getTimeout());
  }

  public String getText(int sec) {
    return getVisibleElement(sec).getText();
  }

  public boolean isVisible() {
    return isVisible(driver.getTimeout());
  }

  public boolean isVisible(int sec) {
    try {
      WebElement e = driver.getVisibleElement(locator, sec);
      if (e == null) {
        return false;
      }
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  public String getCssValue(String name) {
    return getVisibleElement().getCssValue(name);
  }

  public String getAttributeValue(String name) {
    return getVisibleElement().getAttribute(name);
  }

  public void waitCssChanged(String attribute, String curValue) {
    driver.waitCssChanged(locator, attribute, curValue);
  }

  public void waitAttributeChanged(String attribute, String curValue) {
    driver.waitAttributeChanged(locator, attribute, curValue);
  }

  public String waitAttributeNotChanged(String attribute) {
    return driver.waitAttributeNotChanged(locator, attribute);
  }

  public String waitTextNotChanged() {
    return driver.waitTextNotChanged(locator);
  }
}