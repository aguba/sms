package controls;

import drivers.BaseDriver;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ListControl<T extends BaseControl> {

  protected String name;
  protected By locator;
  protected BaseDriver driver;
  protected Class<T> clazz;

  public ListControl(String name, By locator, BaseDriver driver, Class<T> clazz) {
    this.name = name;
    this.locator = locator;
    this.driver = driver;
    this.clazz = clazz;
  }

  public List<WebElement> getElements() {
    return driver.getVisibleElements(locator);
  }

  public List<String> getTexts() {
    return getElements().stream().map(e -> e.getText()).collect(Collectors.toList());
  }
}
