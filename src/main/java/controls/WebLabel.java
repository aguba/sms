package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;

public class WebLabel extends BaseControl {

  public WebLabel(String name, By locator, BaseDriver driver) {
    super(name, locator, driver);
  }

  public WebLabel(String name, String locator, BaseDriver driver) {
    super(name, By.xpath(locator), driver);
  }
}