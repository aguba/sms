package controls;

import drivers.BaseDriver;
import org.openqa.selenium.By;

public class WebLayer extends BaseControl {

  public WebLayer(String name, By locator, BaseDriver driver) {
    super(name, locator, driver);
  }

  public WebLayer(String name, String locator, BaseDriver driver) {
    super(name, By.xpath(locator), driver);
  }
}