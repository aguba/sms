package drivers;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.SleepUtil;
import waiters.WebWaiter;

public class BaseDriver {

  protected WebDriver webDriver;
  protected WebWaiter wait;
  protected int timeout = 10;

  public BaseDriver() {
    wait = new WebWaiter();
  }

  public void navigateTo(String url) {
    try {
      this.webDriver.get(url);
    } catch (TimeoutException e) {
    }
  }

  public WebDriver getWebDriver() {
    return webDriver;
  }

  public WebWaiter getWait() {
    return wait;
  }

  public JavascriptExecutor getJsDriver() {
    return (JavascriptExecutor) webDriver;
  }

  public int getTimeout() {
    return timeout;
  }

  public void verifyTitle(String title) {
    wait.verifyTitle(title);
  }

  public WebElement getVisibleElement(By locator) {
    return getVisibleElement(locator, timeout);
  }

  public WebElement getVisibleElement(By locator, int sec) {
    return wait.getVisibleElement(locator, sec);
  }

  public List<WebElement> getVisibleElements(By locator) {
    return getVisibleElements(locator, timeout);
  }

  public List<WebElement> getVisibleElements(By locator, int sec) {
    return wait.getVisibleElements(locator, sec);
  }

  public WebElement getClickableElement(By locator, int sec) {
    return wait.getClickableElement(locator, sec);
  }

  public void waitCssChanged(By locator, String attribute, String curValue) {
    wait.waitCssChanged(locator, attribute, curValue);
  }

  public void waitAttributeChanged(By locator, String attribute, String curValue) {
    wait.waitAttributeChanged(locator, attribute, curValue);
  }

  public String waitAttributeNotChanged(By locator, String attribute) {
    while (true) {
      String curValue = wait.getElement(locator).getAttribute(attribute);
      SleepUtil.sleep(1);
      String newValue = wait.getElement(locator).getAttribute(attribute);
      if (newValue.equals(curValue)) {
        return newValue;
      }
    }
  }

  public String waitTextNotChanged(By locator) {
    String curValue = wait.getVisibleElement(locator, timeout).getText();
    while (true) {
      SleepUtil.sleep(1);
      String newValue = wait.getVisibleElement(locator, timeout).getText();
      if (newValue.equals(curValue)) {
        return newValue;
      }
      curValue = newValue;
    }
  }
}
