package drivers;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverWrapper extends BaseDriver {
  public ChromeDriverWrapper() {
    this.webDriver = new ChromeDriver();
    wait.init(this);
  }
}
