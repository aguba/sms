package entities;

public class PrizeRow {

  public static String name = "prize_";
  private String rowId;
  private int reel1;
  private int reel2;
  private int reel3;

  public PrizeRow(int reel1, int reel2, int reel3) {
    this.reel1 = reel1;
    this.reel2 = reel2;
    this.reel3 = reel3;
  }

  public int getReel1() {
    return reel1;
  }

  public int getReel2() {
    return reel2;
  }

  public int getReel3() {
    return reel3;
  }

  public String getRowId() {
    return rowId;
  }

  public void setRowId(String rowId) {
    this.rowId = rowId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PrizeRow prizeRow = (PrizeRow) o;

    if (reel1 != prizeRow.reel1) {
      return false;
    }
    if (reel2 != prizeRow.reel2) {
      return false;
    }
    return reel3 == prizeRow.reel3;

  }

  @Override
  public int hashCode() {
    int result = reel1;
    result = 31 * result + reel2;
    result = 31 * result + reel3;
    return result;
  }

  @Override
  public String toString() {
    return "PrizeRow{" +
        "reel1=" + reel1 +
        ", reel2=" + reel2 +
        ", reel3=" + reel3 +
        '}';
  }
}
