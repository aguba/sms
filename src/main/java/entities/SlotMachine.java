package entities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SlotMachine {

  public static int REEL_NUMBER = 3;
  public static int START_POSITION = -634;
  public static int DISTANCE = 120;
  private Map<PrizeRow, Integer> prizes = new HashMap<>();

  public void addPrize(PrizeRow prizeRow, int payout) {
    prizes.put(prizeRow, payout);
  }

  public void addPriceRows(String rowId, Set<List<Integer>> normalizedRows, Integer payout) {
    for (List<Integer> normalizedRow : normalizedRows) {
      PrizeRow row = new PrizeRow(normalizedRow.get(0), normalizedRow.get(1),
          normalizedRow.get(2));
      row.setRowId(rowId);
      addPrize(row, payout);
    }
  }

  public Map<PrizeRow, Integer> getPrizes() {
    return this.prizes;
  }
}
