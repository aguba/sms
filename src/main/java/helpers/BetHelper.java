package helpers;

import drivers.BaseDriver;
import enums.Directions;
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import pages.HomePage;

public class BetHelper {

  private BaseDriver driver;

  public BetHelper(BaseDriver driver) {
    this.driver = driver;
  }

  public BetHelper changeBetAndVerifyPayout(Directions direction) {
    HomePage homePage = new HomePage(driver);
    int currBet = homePage.getBet();
    List<Integer> currPayouts = homePage.getAllPayouts();

    List<Integer> oneBetPayouts = new ArrayList<>();
    for (int i = 0; i < currPayouts.size(); i++) {
      oneBetPayouts.add(currPayouts.get(i) / currBet);
    }
    int newBet;
    if (direction.equals(Directions.UP)) {
      newBet = homePage.increaseBet().getBet();
    } else {
      newBet = homePage.decreaseBet().getBet();
    }

    List<Integer> newPayouts = homePage.getAllPayouts();

    List<Integer> expectedPayouts = new ArrayList<>();
    for (int i = 0; i < currPayouts.size(); i++) {
      expectedPayouts.add(currPayouts.get(i) + (newBet - currBet) * oneBetPayouts.get(i));
    }

    Assert.assertEquals(expectedPayouts, newPayouts, "Verify payouts increased properly");
    return this;
  }

  public HomePage increaseBetUntilMaximumReached() {
    HomePage homePage = new HomePage(driver);
    int curBet;
    do {
      curBet = homePage.getBet();
      homePage.increaseBet();
    } while (homePage.getBet() != curBet);
    return homePage;
  }

}
