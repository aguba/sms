package helpers;

import controls.WebLayer;
import drivers.BaseDriver;
import entities.SlotMachine;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.HomePage;
import utils.MatcherUtil;
import utils.PrizeUtil;

public class PrizeHelper {

  private BaseDriver driver;

  public PrizeHelper(BaseDriver driver) {
    this.driver = driver;
  }

  public SlotMachine getPrizes() {
    HomePage homePage = new HomePage(driver);
    List<WebElement> prizeRows = homePage.getPrizeRows();
    List<Integer> payouts = homePage.getAllPayouts();
    Assert
        .assertEquals(prizeRows.size(), payouts.size(), "Verify payouts number the same as prices");

    SlotMachine slotMachine = new SlotMachine();
    for (int i = 0; i < prizeRows.size(); i++) {
      String rowId = prizeRows.get(i).getAttribute("id");
      List<Set<Integer>> row = new ArrayList<>();
      for (int j = 1; j <= SlotMachine.REEL_NUMBER; j++) {
        WebLayer prizeReel = new WebLayer("Prize Reel",
            By.xpath(String.format(HomePage.REEL_PRIZE_XPATH, i + 1, j)), driver);
        try {
          String val = MatcherUtil.match("prize_(.+$)", prizeReel.getAttributeValue("class"), 1);
          row.add(Arrays.asList(val.split("slash")).stream().map(e -> Integer.parseInt(e))
              .collect(Collectors.toSet()));
        } catch (NumberFormatException e) {
        }
      }
      if (row.size() == SlotMachine.REEL_NUMBER) {
        Set<List<Integer>> normalizedRows = PrizeUtil.normalize(row);
        slotMachine.addPriceRows(rowId, normalizedRows, payouts.get(i));
      }
    }

    return slotMachine;
  }

}
