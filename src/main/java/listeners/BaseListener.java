package listeners;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class BaseListener extends TestListenerAdapter {

  @Override
  public void onTestSuccess(ITestResult testResult) {
    log(testResult.getTestClass().getName() + " -- Passed\n");
  }

  @Override
  public void onTestFailure(ITestResult testResult) {
    log(testResult.getTestClass().getName() + " -- Failed\n");
  }

  @Override
  public void onTestSkipped(ITestResult testResult) {
    log(testResult.getTestClass().getName() + " -- Skipped\n");
  }

  private void log(String string) {
    System.out.println(string);
  }
}
