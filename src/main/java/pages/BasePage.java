package pages;

import drivers.BaseDriver;

public class BasePage {
  protected final BaseDriver driver;
  public BasePage(BaseDriver driver, String title) {
    this.driver = driver;
    driver.verifyTitle(title);
  }

}
