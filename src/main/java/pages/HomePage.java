package pages;

import controls.ListControl;
import controls.WebButton;
import controls.WebImage;
import controls.WebLabel;
import controls.WebLayer;
import drivers.BaseDriver;
import entities.PrizeRow;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.MatcherUtil;
import utils.PrizeUtil;
import utils.SleepUtil;

public class HomePage extends BasePage {

  private static String TITLE = "Add a HTML5 Slot Machine to your Site";
  private static String PRIZES_MACHINE_XPATH = "//div[@class='prizes_list_slot_machine' and not(contains(@style,'display: none'))]";
  private static String PRIZES_ROW_XPATH =
      PRIZES_MACHINE_XPATH + "//div[contains(@class,'trPrize')]";
  public static String REEL_PRIZE_XPATH =
      "(" + PRIZES_ROW_XPATH + ")[%s]//div[contains(@class, 'reel%s')]";
  private final WebLabel lastWin, totalSpins, bet, dayWinnings, lifetimeWinnings;
  private final WebButton betSpinUp, betSpinDown, spin;
  private final WebImage reel1, reel2, reel3;
  private final WebButton changeBackground, changeIcons, changeMachine;
  private final WebImage currentBackground;
  private final WebLayer slots, slotsOuterContainer;
  private final ListControl<WebLabel> payouts;

  public HomePage(BaseDriver driver) {
    super(driver, TITLE);
    lastWin = new WebLabel("Last Win", By.id("lastWin"), driver);
    totalSpins = new WebLabel("Total Spins", By.id("credits"), driver);
    dayWinnings = new WebLabel("Day Winnings", By.id("dayWinnings"), driver);
    lifetimeWinnings = new WebLabel("Lifetime Winnings", By.id("lifetimeWinnings"), driver);
    bet = new WebLabel("Bet", By.id("bet"), driver);
    betSpinUp = new WebButton("Spin Up", By.id("betSpinUp"), driver);
    betSpinDown = new WebButton("Spin Down", By.id("betSpinDown"), driver);
    spin = new WebButton("Spin Button", By.id("spinButton"), driver);
    reel1 = new WebImage("Reel1", By.id("reel1"), driver);
    reel2 = new WebImage("Reel2", By.id("reel2"), driver);
    reel3 = new WebImage("Reel3", By.id("reel3"), driver);
    changeBackground = new WebButton("Change Background", By.className("btnChangeBackground"),
        driver);
    changeIcons = new WebButton("Change Icons", By.className("btnChangeReels"),
        driver);
    changeMachine = new WebButton("Change Machine", By.className("btnChangeMachine"),
        driver);
    currentBackground = new WebImage("Current Background",
        "//div[contains(@id,'changeable_background') and not(contains(@style,'display: none'))]",
        driver);
    slots = new WebLayer("Slots Wrapper", By.id("slotsSelectorWrapper"), driver);
    slotsOuterContainer = new WebLayer("Won Container", By.cssSelector("#SlotsOuterContainer.won"),
        driver);
    payouts = new ListControl<>("Payouts", By.xpath(
        PRIZES_MACHINE_XPATH + "//span[@class='tdPayout']"),
        driver, WebLabel.class);

  }

  public String getBackground() {
    return MatcherUtil.match("\"(.*?)\"", currentBackground.getCssValue("background-image"), 1);
  }

  public Integer getIconSet() {
    return Integer
        .parseInt(MatcherUtil.match("reelSet(\\d+)", slots.getAttributeValue("class"), 1));
  }

  public HomePage clickChangeBackground() {
    String background = getBackground();
    changeBackground.click();
    currentBackground.waitCssChanged("background-image", background);
    return this;
  }

  public HomePage clickChangeIcons() {
    String classValue = slots.getAttributeValue("class");
    changeIcons.click();
    slots.waitAttributeChanged("class", classValue);
    return this;
  }

  public HomePage clickChangeMachine() {
    String classValue = slots.getAttributeValue("class");
    changeMachine.click();
    slots.waitAttributeChanged("class", classValue);
    return this;
  }

  public int getLastWin() {
    String win = lastWin.getText(1);
    return StringUtils.isBlank(win) ? 0 : Integer.parseInt(win);
  }

  public int getBet() {
    return Integer.parseInt(bet.getText());
  }

  public int getCredits() {
    return Integer.parseInt(totalSpins.getText());
  }

  public int waitAndGetCredits() {
    return Integer.parseInt(totalSpins.waitTextNotChanged());
  }

  public HomePage decreaseBet() {
    betSpinDown.click();
    SleepUtil.sleep(1);
    return this;
  }

  public HomePage increaseBet() {
    betSpinUp.click();
    SleepUtil.sleep(1);
    return this;
  }

  public List<Integer> getAllPayouts() {
    return payouts.getTexts().stream().map(t -> Integer.parseInt(t)).collect(Collectors.toList());
  }

  public List<WebElement> getPrizeRows() {
    return new ListControl<>("Prizes Rows", By.xpath(PRIZES_ROW_XPATH),
        driver, WebLayer.class)
        .getElements();
  }


  public PrizeRow spinAndGetPrize() {
    spin.click();
    Integer top1 = getTopValue(reel1.waitAttributeNotChanged("style"));
    Integer top2 = getTopValue(reel2.waitAttributeNotChanged("style"));
    Integer top3 = getTopValue(reel3.waitAttributeNotChanged("style"));
    return PrizeUtil.getPrize(top1, top2, top3);

  }

  private Integer getTopValue(String style) {
    return Integer.parseInt(MatcherUtil.match("top:(-?\\d+)px", style.replaceAll(" ", ""), 1));
  }

  public boolean isWonContainerVisible() {
    return slotsOuterContainer.isVisible(1);
  }

  public boolean isAnyPrizeRowWon() {
    return new WebLayer("Prize Row Won", By.className("trPrize won"), driver).isVisible(1);
  }

  public boolean isRowWon(String id) {
    return new WebLayer("Prize row " + id + " Won", By.cssSelector("#" + id + ".trPrize.won"),
        driver).isVisible();
  }

  public Integer getMachine() {
    return Integer
        .parseInt(MatcherUtil.match("slotMachine(\\d+)", slots.getAttributeValue("class"), 1));
  }
}
