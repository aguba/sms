package tests;

import drivers.BaseDriver;
import drivers.ChromeDriverWrapper;
import org.testng.annotations.AfterMethod;
import pages.HomePage;

public class BaseTest {

  private static final String HOME_URL = "http://slotmachinescript.com";
  private BaseDriver driver;

  @AfterMethod(alwaysRun = true)
  public void afterMethod() {
    if (driver != null) {
      driver.getWebDriver().quit();
      driver = null;
    }
  }

  protected BaseDriver getDriver() {
    if (driver == null) {
      driver = new ChromeDriverWrapper();
    }
    return driver;
  }

  public HomePage navigateHomePage() {
    getDriver().navigateTo(HOME_URL);
    return new HomePage(driver);
  }

}
