package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherUtil {

  public static String match(String pattern, String match, int index) {
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(match);
    if (m.find()) {
      return m.group(index);
    }
    return null;
  }

}
