package utils;

import com.google.common.collect.Sets;
import entities.PrizeRow;
import entities.SlotMachine;
import java.util.List;
import java.util.Set;

public class PrizeUtil {

  public static Set<List<Integer>> normalize(List<Set<Integer>> prizeRow) {
    return Sets.cartesianProduct(prizeRow);
  }

  public static PrizeRow getPrize(int coord1, int coord2, int coord3) {
    return new PrizeRow(convertCoordToPrizeId(coord1), convertCoordToPrizeId(coord2),
        convertCoordToPrizeId(coord3));
  }

  private static int convertCoordToPrizeId(int coord) {
    return (SlotMachine.START_POSITION - coord) / SlotMachine.DISTANCE + 1;
  }

}
