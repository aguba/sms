package utils.rest;

import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class RestHttpClient {

  public static RestResponse post(String url, int bet) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpPost httpPost = new HttpPost(url);
    httpPost.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
    httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
    String requestBody = "bet=" + bet;
    try {
      httpPost.setEntity(new ByteArrayEntity(requestBody.getBytes("UTF-8")));
      CloseableHttpResponse response = httpclient.execute(httpPost);
      String body = StringUtils
          .join(IOUtils.readLines(response.getEntity().getContent(), Charset.defaultCharset()), "");
      return new RestResponse(response.getStatusLine().getStatusCode(), body);
    } catch (Exception e) {
    }
    return null;
  }
}
