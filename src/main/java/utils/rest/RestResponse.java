package utils.rest;

public class RestResponse {

  private int status;
  private String body;

  public RestResponse(int status, String body) {
    this.status = status;
    this.body = body;
  }

  public int getStatus() {
    return status;
  }

  public String getBody() {
    return body;
  }
}
