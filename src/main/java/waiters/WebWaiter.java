package waiters;

import drivers.BaseDriver;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class WebWaiter {

  private BaseDriver driver;
  private FluentWait<WebDriver> wait;

  public void init(BaseDriver driver) {
    this.driver = driver;
    wait = new FluentWait<>(driver.getWebDriver());
    wait.pollingEvery(200, TimeUnit.MILLISECONDS);
    wait.withTimeout(10, TimeUnit.SECONDS);
    wait.ignoring(NoSuchElementException.class);
  }

  public WebElement getElement(By locator) {
    return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
  }

  public List<WebElement> getElements(By locator) {
    return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
  }

  public WebElement getVisibleElement(By locator, int sec) {
    wait.withTimeout(sec, TimeUnit.SECONDS);
    return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
  }

  public List<WebElement> getVisibleElements(By locator, int sec) {
    wait.withTimeout(sec, TimeUnit.SECONDS);
    return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
  }

  public WebElement getClickableElement(By locator, int sec) {
    wait.withTimeout(sec, TimeUnit.SECONDS);
    return wait.until(ExpectedConditions.elementToBeClickable(locator));
  }

  public void waitCssChanged(By locator, String css, String value) {
    wait.until(new ExpectedCondition<Boolean>() {
      public Boolean apply(WebDriver driver) {
        WebElement e = getElement(locator);
        String actual = e.getCssValue(css);
        if (actual.contains(value)) {
          return false;
        }
        return true;
      }
    });
  }

  public void waitTextChanged(By locator, String value) {
    wait.until(new ExpectedCondition<Boolean>() {
      public Boolean apply(WebDriver driver) {
        WebElement e = getElement(locator);
        String actual = e.getText();
        if (actual.equals(value)) {
          return false;
        }
        return true;
      }
    });
  }

  public void waitAttributeChanged(By locator, String name, String value) {
    wait.until(ExpectedConditions.not(ExpectedConditions.attributeToBe(locator, name, value)));
  }

  public boolean verifyTitle(String title) {
    return wait.until(ExpectedConditions.titleIs(title));
  }

}
