package testcases.customizable;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import tests.BaseTest;

public class ChangeBackgroundTest extends BaseTest {

  @Test(groups = {"customizable"})
  public void verifyChange() {
    HomePage homePage = navigateHomePage();
    String currBackground = homePage.getBackground();
    String newBackground = homePage.clickChangeBackground().getBackground();
    Assert.assertNotEquals(currBackground, newBackground, "Verify that background changed.");
  }

}
