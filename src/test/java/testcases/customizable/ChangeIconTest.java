package testcases.customizable;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import tests.BaseTest;

public class ChangeIconTest extends BaseTest {

  @Test(groups = {"customizable"})
  public void verifyChange() {
    HomePage homePage = navigateHomePage();
    int currIconSet = homePage.getIconSet();
    int newIconSet = homePage.clickChangeIcons().getIconSet();
    Assert.assertNotEquals(currIconSet, newIconSet, "Verify that icons changed.");
  }

}
