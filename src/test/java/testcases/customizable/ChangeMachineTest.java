package testcases.customizable;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import tests.BaseTest;

public class ChangeMachineTest extends BaseTest {

  @Test(groups = {"customizable"})
  public void verifyChange() {
    HomePage homePage = navigateHomePage();
    int currMachine = homePage.getMachine();
    int newMachine = homePage.clickChangeMachine().getMachine();
    Assert.assertNotEquals(currMachine, newMachine, "Verify that machine changed.");
  }

}
