package testcases.machine;

import helpers.BetHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class BetGreaterCreditsTest extends BaseTest {

  @Test(groups = {"validation"})
  public void verifyNotGreaterThanCredits() {
    int numberOfCredits = navigateHomePage().getCredits();
    int maximumBet = new BetHelper(getDriver()).increaseBetUntilMaximumReached().getBet();
    Assert.assertTrue(numberOfCredits > maximumBet, "Verify maximum Bet is Less than credits");
  }

}
