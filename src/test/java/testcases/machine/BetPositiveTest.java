package testcases.machine;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import tests.BaseTest;

public class BetPositiveTest extends BaseTest {

  @Test(groups = {"validation"})
  public void verifyAlwaysPositive() {
    HomePage homePage = navigateHomePage();
    Assert.assertEquals(homePage.getBet(), 1, "Verify Bet value = 1");
    Assert.assertEquals(homePage.decreaseBet().getBet(), 1, "Verify bet could NOT be 0");
  }

}
