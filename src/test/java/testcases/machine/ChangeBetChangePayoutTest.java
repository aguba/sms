package testcases.machine;

import enums.Directions;
import helpers.BetHelper;
import org.testng.annotations.Test;
import tests.BaseTest;

public class ChangeBetChangePayoutTest extends BaseTest {

  @Test(groups = {"validation"})
  public void verifyChangePayout() {
    navigateHomePage();
    new BetHelper(getDriver()).changeBetAndVerifyPayout(Directions.UP)
        .changeBetAndVerifyPayout(Directions.DOWN);

  }
}
