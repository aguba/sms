package testcases.machine;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import tests.BaseTest;

public class LastWinTest extends BaseTest {

  @Test(groups = {"validation"})
  public void verifyDefaultIsEmpty() {
    HomePage homePage = navigateHomePage();
    Assert.assertEquals(homePage.getLastWin(), 0, "Last win is empty or zero by default");
  }

}
