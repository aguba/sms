package testcases.machine;

import entities.PrizeRow;
import entities.SlotMachine;
import helpers.PrizeHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import tests.BaseTest;

public class MachineTest extends BaseTest {

  @Test(groups = {"E2E"})
  public void e2eChangeBetAndVerifyWin() {
    HomePage homePage = navigateHomePage();
    homePage.increaseBet();
    SlotMachine slotMachine = new PrizeHelper(getDriver()).getPrizes();

    while (true) {
      int credits = homePage.getCredits();
      int bet = homePage.getBet();
      PrizeRow resultRow = homePage.spinAndGetPrize();
      Integer payout = slotMachine.getPrizes().get(resultRow);
      if (payout == null) {
        Assert.assertEquals(homePage.getCredits(), credits - bet, "Verify credits decrease on bet");
        Assert.assertFalse(homePage.isWonContainerVisible(), "Verify WON is NOT visible");
        Assert.assertFalse(homePage.isAnyPrizeRowWon(), "Verify NO rows WON");
      } else {
        PrizeRow prizeRow = slotMachine.getPrizes().keySet().stream()
            .filter(o -> o.equals(resultRow)).findFirst().get();
        Assert.assertEquals(homePage.waitAndGetCredits(), credits - bet + payout,
            "Verify credits increase on payout");
        Assert.assertTrue(homePage.isWonContainerVisible(), "Verify WON is NOT visible");
        Assert.assertTrue(homePage.isRowWon(prizeRow.getRowId()), "Verify row WON");
        Assert.assertEquals(homePage.getLastWin(), payout.intValue(), "Verify row WON");
        return;
      }
    }
  }

}
