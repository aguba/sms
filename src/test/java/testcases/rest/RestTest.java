package testcases.rest;

import entities.SlotMachine;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.rest.RestHttpClient;
import utils.rest.RestResponse;

public class RestTest {

  private String url = "http://slotmachinescript.com/slots/spin.php";

  @Test(groups = {"Rest"})
  public void verifyResponse() {
    RestResponse response = RestHttpClient.post(url, 1);
    Assert.assertEquals(response.getStatus(), 200, "Verify response OK");
    JSONObject content = new JSONObject(response.getBody());
    Assert.assertEquals(content.getJSONArray("reels").length(), SlotMachine.REEL_NUMBER,
        "Verify reels length is the same as on UI");
    Assert.assertTrue(content.has("prize"), "Verify prize is present in response");
  }

}
